#!/usr/bin/env python3

import os
import re
import sys
from jinja2 import FileSystemLoader, Environment
from pynetbox import api
from pynetbox.core.api import Api
from pynetbox.core.response import Record
from pynetbox.models.dcim import Devices
from pynetbox.models.virtualization import VirtualMachines
from yaml import dump


def get_role(host):
    if isinstance(host, Devices):
        return host['device_role']['slug']
    return host['role']['slug']

def get_site(host):
    return host['site']['slug']

def get_local_metro(site: Record):
    metro_tags = [
        '(' + t['name'].split('::')[1].replace('_', ',') + ')'
        for t in site.tags
        if t['name'].split('::')[0] == 'LOCAL_METRO'
    ]

    if not metro_tags:
        return None

    if len(metro_tags) > 1:
        print(f'Site "{site.name}" has more than one LOCAL_METRO tag', file=sys.stderr)
        exit(2)

    return metro_tags[0]


def has_tag(host, tag: str):
    return bool(list(filter(lambda t: t['slug'] == tag, host['tags'])))


def is_bgp_host(host):
    return is_bgp_rr_client(host) or get_role(host) in ['core-router', 'global-route-reflector']


def is_bgp_rr_client(host):
    if get_role(host) in ['core-router', 'global-route-reflector']:
        return False
    return get_role(host) in ['hypervisor'] or has_tag(host, 'routing-bgp')


def host_from_record(record):
    if 'virtual_machine' in dict(record):
        return record['virtual_machine']

    return record['device']


def generate(netbox: Api, directory: str, only_hosts: list):
    hardware = netbox.dcim.devices.all()
    vms = netbox.virtualization.virtual_machines.all()
    hardware_interfaces = netbox.dcim.interfaces.all()
    vm_interfaces = netbox.virtualization.interfaces.all()
    all_ips = netbox.ipam.ip_addresses.all()

    hosts = list(hardware) + list(vms)
    hosts = list(filter(lambda h: h['primary_ip'] is not None and h['status']['value'] != 'offline', hosts))  # Has an IP
    bgp_hosts = [h for h in hosts if h['tenant'] and h['tenant']['name'] == 'AS64475' and is_bgp_host(h)]

    sites = {
        site.slug: {
            'site': site,
            'rr_bgp': [],
            'rr': [],
            'local_metro': get_local_metro(site),
        } for site in netbox.dcim.sites.all()
    }

    all_interfaces = list(hardware_interfaces) + list(vm_interfaces)
    interfaces = {h['name']: {} for h in hosts}

    global_rr = []
    global_rr_client = []
    for host in bgp_hosts:
        role = get_role(host)
        if is_bgp_rr_client(host):
            global_rr_client.append(host)
        if role == 'global-route-reflector':
            global_rr.append(host)
        if role == 'core-router':
            global_rr.append(host)

    all_ips = list(all_ips)
    ip_regex = re.compile(r".*?(\d+\.\d+\.\d+\.\d+|(?:[a-f\d]+::?)+[a-f\d]*)")
    for host in hosts:
        host_interfaces = [
            i
            for i in all_interfaces
            if host_from_record(i)['name'] == host['name'] and ('virtual_machine' in dict(i) or not i['mgmt_only'])
        ]
        for interface in host_interfaces:
            ips = [i for i in all_ips if i['assigned_object'] and i['assigned_object']['url'] == interface['url']]
            interface_type = [t['name'].split('::')[1] for t in interface['tags'] if 'type::' in t['name']]
            ospf_cost = [int(t['name'].split('::')[1]) for t in interface['tags'] if 'ospf-cost::' in t['name']]

            vlan = None
            if interface['mode'] and interface['mode']['value'] == 'access':
                vlan = interface['untagged_vlan']['vid']

            vlan_match = re.match(r".+\.(\d+)", interface['name'])
            if not vlan and vlan_match:
                vlan = int(vlan_match.group(1))

            link = None
            if 'link_peer_type' in dict(interface) and interface['link_peer_type'] == 'circuits.circuittermination':
                circuit_type = interface.link_peer.circuit.type.name
                local_ip = ip_regex.match(interface.description)
                remote_ip = interface.link_peer.circuit.termination_a.link_peer.description
                remote_ip = ip_regex.match(remote_ip)
                if remote_ip and local_ip and remote_ip.group(1) == local_ip.group(1):
                    remote_ip = interface.link_peer.circuit.termination_z.link_peer.description
                    remote_ip = ip_regex.match(remote_ip)

                if remote_ip and local_ip:
                    link = {'type': circuit_type, 'local_ip': local_ip.group(1), 'remote_ip': remote_ip.group(1)}

            bond_members = None
            is_lag = 'type' in dict(interface) and interface['type']['value'] == 'lag'
            if is_lag:
                bond_members = []
                for i in host_interfaces:
                    if not i['lag'] or i.lag.id != interface.id:
                        continue
                    bond_members.append(i.name)

            lag_interface = None
            if 'lag' in dict(interface) and interface['lag']:
                lag_interface = interface.lag.name

            parent_interface = None
            if 'parent' in dict(interface) and interface['parent']:
                parent_interface = interface.parent.name

            data = {
                'enabled': interface['enabled'],
                'addresses': ips,
                'mtu': interface['mtu'],
                'vlan': vlan,
                'ospf_cost': None if not ospf_cost else ospf_cost[0],
                'type': None if not interface_type else interface_type[0],
                'bond_members': bond_members,
                'bond_related': is_lag or 'lag' in dict(interface),
                'lag_interface': lag_interface,
                'parent_interface': parent_interface,
                'link': link,
            }

            interfaces[host['name']][interface['name']] = data

    if only_hosts:
        # Is part of the hosts list
        hosts = list(filter(lambda h: h['name'] in only_hosts, hosts))
        interfaces = dict(filter(lambda h: h[0] in only_hosts, interfaces.items()))

    env = Environment(loader=FileSystemLoader(searchpath='./'))
    env.filters['ip'] = lambda ip: ip['address'].split('/')[0]
    env.filters['yaml'] = dump
    env.globals.update(get_role=get_role)
    env.globals.update(get_site=get_site)
    template = env.get_template('pillar.j2')

    for host in hosts:
        path = directory + '/' + '/'.join(host['name'].split('.')[::-1])
        file = path + '/netbox.sls'

        if has_tag(host, 'no-pillar') and host.name not in only_hosts:
            continue

        print('Generating ' + file)
        output = template.render({
            'host': host,
            'is_vm': isinstance(host, VirtualMachines),
            'cluster': host['cluster']['name'] if host['cluster'] else None,
            'cluster_type': host.cluster.type.slug if host['cluster'] else None,
            'cluster_group': host.cluster.group.slug if host['cluster'] else None,
            'role': get_role(host),
            'sites': sites,
            'site': sites[host.site.slug],
            'is_bgp': is_bgp_host(host),
            'global_rr': global_rr,
            'global_rr_client': global_rr_client,
            'interfaces': interfaces[host['name']]
        })

        os.makedirs(path, exist_ok=True)
        f = open(file, 'w')
        f.write(output)
        f.close()


if __name__ == '__main__':
    if 'NETBOX_TOKEN' not in os.environ:
        print('NETBOX_TOKEN env not found', file=sys.stderr)
        exit(2)

    if len(sys.argv) < 2:
        print('Path to pillars host/ dir missing', file=sys.stderr)
        exit(2)

    only_hosts = []
    if 'HOSTS' in os.environ and os.environ['HOSTS']:
        only_hosts = os.environ['HOSTS'].split(' ')

    directory = sys.argv[1]
    n = api('https://netbox.as64475.net', token=os.environ['NETBOX_TOKEN'])

    generate(n, directory, only_hosts)
