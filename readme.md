# Create pillar files from NetBox
## Env variables
* `$NETBOX_TOKEN`: (Required) Netbox API token
* `$HOSTS` (Optional) List of hosts that should be generated (default: all)

### CI
* `$PILLAR_REPOSITORY` the repository where pillars live
* `$SSH_PRIVATE_KEY` ssh key to access the repo
